import { Routes, RouterModule } from "@angular/router";
import { StudentsComponent } from './list/students.component';
import { StudentsAddComponent } from './add/students.add.component';
import { StudentsViewComponent } from './view/students.view.component';
import { NgModule } from '@angular/core';
import { FileNotFoundComponent } from '../shared/file-not-found/file-not-found.component';


const StudentRoutes: Routes = [
    { path: 'view', component: FileNotFoundComponent },
    { path: 'add', component: StudentsAddComponent },
    { path: 'list', component: StudentsComponent },
    { path: 'detail/:id', component: StudentsViewComponent },
  
   
];

@NgModule({
    imports: [RouterModule.forRoot(StudentRoutes)],
    exports: [RouterModule],
})

export class StudentRoutingModule { }