import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import Student from '../../entity/student';
import { StudentService } from 'src/app/service/student-service';

@Component({
  selector: 'app-students-view',
  templateUrl: './students.view.component.html',
  styleUrls: ['./students.view.component.css']
})
export class StudentsViewComponent implements OnInit {

  student: Student;

  constructor(private route: ActivatedRoute, private studentService: StudentService) {

  }

  ngOnInit(): void {
    this.student = {
      "id": 1,
      "studentId": "SE-001",
      "name": "",
      "surname": "",
      "gpa": 0,
      "image": "assets/images/file-not-found.jpg",
      "featured": false,
      "penAmount": 0,
      "description": "This is a defeault information."
    }

    this.route.params
      .subscribe((params: Params) => {
        this.studentService.getStudent(+params['id'])
          .subscribe((inputStudent: Student) => {
            this.student = inputStudent
          });
      });
  }
}

