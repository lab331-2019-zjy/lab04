import { Component } from '@angular/core';
import Student from '../../entity/student';

@Component({
  selector: 'app-students-add',
  templateUrl: './students.add.component.html',
  styleUrls: ['./students.add.component.css']
})

export class StudentsAddComponent {
  students: Student[];

 
  penAmount: number =  0;
  

  upQuantity(student: Student) {
    this.penAmount++;
  }

  downQuantity(student: Student) {
    if (this.penAmount > 0) {
      this.penAmount--;
    }
  }

}
